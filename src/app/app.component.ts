import { Component ,OnInit,ElementRef} from '@angular/core';
import {FormGroup, FormControl} from '@angular/forms';
import { parse } from 'querystring';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // title = 'app';
  // department;
  // emp;
  // leader;
  // deptdata:any=[];
  // empdata:any=[];
  // leaderdata:any=[];
  // deptToUpdate = -1;
  // empToUpdate = -1;
  // leaderToUpdate = -1;
  // data = {};

 
  // ngOnInit(){
  //   this.department = new FormGroup({
  //     dept: new FormControl(),
  //     leaderID: new FormControl()   
  //   });
  //   this.emp = new FormGroup({
  //     empname: new FormControl(),
  //     deptID: new FormControl()
  //   });
  //   this.leader = new FormGroup({
  //     teamleader: new FormControl()
  //   });
  // }

  // findLeaderById(id){
  //   id = parseInt(id);
  //   return this.leaderdata.find(leader => {
  //     return leader.id == id;
  //   })
  // }
  // public addDept(dept){
  //   dept.leader = this.findLeaderById(dept.leaderID);
  //   if(this.deptdata.length == 0)
  //   {
  //     this.data = {id:1,dept};
  //     this.deptdata.push(this.data);
  //   }
  //   else{
  //     let newID = this.deptdata.length +1
  //     this.data = {id:newID,dept};
  //     this.deptdata.push(this.data);
  //   }
  //   this.department.reset();
  //   }
  // findDeptById(id) {
  //   id = parseInt(id);
  //   return this.deptdata.find(dept => {
  //     return dept.id == id;
  //   })
  // }
  
  // public addEmp(emp){
  //   emp.dept = this.findDeptById(emp.deptID);
  //   this.empdata.push(emp);
  //   console.log(this.empdata);
  //   this.emp.reset();
  // }

  // public addLeader(leader){
    
  //   if(this.leaderdata.length == 0){
  //     let leader_data ={id:1, leader:leader.teamleader};
  //     this.leaderdata.push(leader_data);
  //     this.leader.reset();
  //   }
  //   else{
  //     let newID = this.leaderdata.length +1;
  //     let leader_data ={id:newID, leader:leader.teamleader};
  //     this.leaderdata.push(leader_data);
  //     this.leader.reset();
  //   }    
  // }

  // public editDept(id){
  //   let data =  this.findDeptById(id)
  //   this.deptToUpdate = id;
  //   this.department.setValue({dept:data.dept.dept,
  //     leaderID:data.dept.leaderID
  //   });
  // }

  // public updateDept(value){
  //     this.deptdata = this.deptdata.map((dept, index) => {
  //       if (dept.id === this.deptToUpdate) {
  //         this.deptdata[index].dept.dept = value.dept;
  //         this.deptdata[index].dept.leaderID = value.leaderID;
  //         this.deptdata[index].dept.leader = this.findLeaderById(value.leaderID);
  //       }
  //       return dept;
  //     });
      
  //     this.department.reset();

  // }

  // public editEmp(index){
  //   this.empToUpdate = index;
  //   this.emp.setValue({empname:this.empdata[index].empname,
  //     deptID:this.empdata[index].deptID
  //   });
  // }

  // public updateEmp(value){
  //   if(this.empToUpdate > -1)
  //   {
  //     this.empdata = this.empdata.map((emp, index)=>{
  //       if(index === this.empToUpdate){
  //         emp.empname = value.empname;
  //         emp.deptID = value.deptID,
  //         emp.dept = this.findDeptById(value.deptID)
  //       }
  //       return emp;
  //     });
  //     this.emp.reset();
  //   }
  //   else{
  //     alert("Please select a dept to update.");
  //   }
  // }
  // public editLeader(index){
  //   this.leaderToUpdate = index;
  //   this.leader.setValue({teamleader:this.leaderdata[index].leader});
  // }

  // public updateLeader(value){
  //   if(this.leaderToUpdate > -1){
  //     this.leaderdata = this.leaderdata.map((leader,index) => {
  //       if(index === this.leaderToUpdate)
  //       {
  //         leader.leader = value.teamleader;
  //       }
  //       return leader;
  //     });
  //   }
  // }
}
