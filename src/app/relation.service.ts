import { Injectable } from '@angular/core';
import { department } from './model/department';

@Injectable()
export class RelationService {
  
public empdata:any=[];
public deptdata:any=[];
public leaderdata:any=[];

  constructor() { }
  
  public saveLeader(detail):Promise<any>{
    return new Promise<any>((response,reject)=>{
      if(this.leaderdata.length == 0){
            let leader_data = {l_id:1, lName:detail.teamleader};
            this.leaderdata.push(leader_data);
          }
          else{
            let newID = this.leaderdata.length +1;
            let leader_data ={l_id:newID, lName:detail.teamleader};
            this.leaderdata.push(leader_data);

          }
          response(this.leaderdata);
    })
  }
  
  public findLeaderById(id){
    id = parseInt(id);
    let leader = this.leaderdata.find(leader => { return leader.l_id == id;})
    return leader;
  }

  public getAllLeaders(){
    return this.leaderdata;
  }

  public updateSaveLeader(update, id):Promise<any>{
    return new Promise<any>((response,reject)=>{
      this.leaderdata.map((leader,index)=>{
        if(leader.l_id == id)
        {
          this.leaderdata[index].lName = update.teamleader; 
        }
      })
      response(this.leaderdata);
    })
  }

  public saveDepartment(detail):Promise<any>{
    return new Promise<any>((response,reject)=>{
      detail.leader = this.findLeaderById(detail.leaderID); 
      if(this.deptdata.length == 0){
        let department_data = {d_id:1, dName:detail.dept, l_id:detail.leaderID};
        this.deptdata.push(department_data);
      }
      else{
        let newID = this.deptdata.length +1;
        let department_data = {d_id:newID, dName:detail.dept, l_id:detail.leaderID};
        this.deptdata.push(department_data);
      }
      response(this.deptdata);
    })
  }

  public getAllDepartment(){
    return this.deptdata;
  }

  public findDeptById(id){
    id = parseInt(id);
    return this.deptdata.find(dept=>{
      return dept.d_id == id;
    })
    
  }
  public updateSaveDepartment(update,id):Promise<any>{
    return new Promise<any>((response,reject)=>{
      this.deptdata.map((dept,index)=>{
        if(dept.d_id == id)
        {
          this.deptdata[index].dName = update.dept;
          this.deptdata[index].l_id = update.leaderID;
        }
      })
      response(this.deptdata);
    })
  }

  public saveEmployee(detail):Promise<any>{
    return new Promise<any>((response,reject)=>{
      let dept = this.findDeptById(detail.deptID);
      if(this.empdata.length == 0){
          let emp_data = {empId:1, empName:detail.empname, depId:dept.d_id, lId: dept.l_id};
          this.empdata.push(emp_data);
        
        }
      else{
          let newID = this.empdata.length +1;
          let emp_data = {empId:newID, empName:detail.empname, depId:dept.d_id, lId:dept.l_id};
          this.empdata.push(emp_data);
        }
      response(this.empdata);
      
    })
  }
  public findEmpById(id){
    const empid = parseInt(id);
    return this.empdata.find(emp =>{
      return emp.empId == id;
    })
  }

  public updateSaveEmployee(update,id):Promise<any>{
    return new Promise<any>((response,reject)=>{
      let dept = this.findDeptById(update.deptID);
      this.empdata.map((emp,index)=>{
        if(emp.empId == id)
        {
          this.empdata[index].empName = update.empname;
          this.empdata[index].depId = dept.d_id;
          this.empdata[index].lId = dept.l_id;
        }
      })
      response(this.empdata);
    })
  }

}