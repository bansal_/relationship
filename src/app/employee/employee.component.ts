import { Component, OnInit, AfterViewChecked} from '@angular/core';
import {FormGroup,FormControl,Validators} from '@angular/forms';
import { employee } from '../model/employee';
import {RelationService} from '../relation.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit, AfterViewChecked {
  
  public emp;
  public empdata:any=[];
  public deptdata:any=[];
  public updateEmployeId;
  public validate:boolean = false;
  public add:boolean = true;
  public update:boolean = false;

  constructor(private relationservice:RelationService) { }

  ngOnInit() {
    this.emp = new FormGroup({
      empname: new FormControl('',Validators.required),
      deptID: new FormControl()
    });
    this.deptdata =  this.relationservice.getAllDepartment();
  }
  ngAfterViewChecked(){
    // if(this.deptdata.length > 0)
    // {
    //   this.emp.patchValue({
    //     deptID:this.deptdata[0].d_id
    //   });
    // }
  }
  public checkValidate(event){
    if(event.target.value){
      this.validate = false;
    }
    else{
      this.validate = true;
    }
  }
  public addEmp(value){
    if(value.empname != '' && value.empname != null)
    {
      this.relationservice.saveEmployee(value).then((result)=>{
        this.empdata = result;
      })
      this.emp.controls.empname.reset();
    }
    else{
      this.validate = true;
    }
  }
  
  public getNameOfDept(id){
    let department = this.relationservice.findDeptById(id)
    return department.dName;
  }
  public getNameOfLeader(id){
    let leader = this.relationservice.findLeaderById(id)
    return leader.lName;
  }
  public editEmp(id){
    let employee = this.relationservice.findEmpById(id);
    this.emp.setValue({empname:employee.empName,
      deptID:employee.depId
    });
    this.add = false;
    this.update = true;
    this.updateEmployeId = id; 
  }

  public updateEmployee(value){
    this.relationservice.updateSaveEmployee(value,this.updateEmployeId).then((result)=>{
      this.empdata = result;
    })
    this.updateEmployeId = null;
    this.add = true;
    this.update = false;
    this.emp.controls.empname.reset();
  }


}
