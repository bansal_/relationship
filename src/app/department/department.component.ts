import { Component, OnInit ,AfterViewChecked} from '@angular/core';
import {FormGroup,FormControl, Validators} from '@angular/forms';
import { department } from '../model/department';
import { RelationService } from '../relation.service';

@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.css']
})
export class DepartmentComponent implements OnInit,AfterViewChecked {

  public department;
  public deptdata:any=[];
  public leaderdata:any=[];
  public updateDepartmentId;
  public validate:boolean = true;
  public add:boolean = true;
  public update:boolean= false; 
  
  constructor(private relationservice:RelationService) { }

  ngOnInit() {
    this.department = new FormGroup({
      dept: new FormControl('',Validators.required ),
      leaderID: new FormControl()
    });
    this.leaderdata = this.relationservice.getAllLeaders();
    
    // this.deptdata = this.relationservice.getAllDepartment();
  }

  ngAfterViewChecked() {
    // if(this.leaderdata.length > 0){
    //   this.department.patchValue({
    //     leaderID: this.leaderdata[0].l_id
    //   });
    // }
  }
  public checkValidate(event){
    if(event.target.value)
    {
       this.validate = true;
    }
    else
    {
      this.validate = false;
    }
  }
  public addDepartment(value){
    if(value.dept != '' && value.dept != null)
    {
      this.relationservice.saveDepartment(value).then((result)=>{
      this.deptdata = result;
    })
      this.department.controls.dept.reset();
    }
    else{
      this.validate = false; 
    } 
  }

  public getLeaderName(id) {
    let leader = this.relationservice.findLeaderById(id)
    return leader.lName;
  }

  public editDept(id){
    const dept = this.relationservice.findDeptById(id);
    this.department.setValue({
      dept:dept.dName,
      leaderID:dept.l_id
    });
    this.update = true;
    this.add = false;
    this.updateDepartmentId = id;
  }

  public updateDepartment(value){
    this.relationservice.updateSaveDepartment(value, this.updateDepartmentId).then((result)=>{
      this.deptdata = result;
    })
    this.update = false;
    this.add = true;
    this.department.controls.dept.reset();
  }

}
