import { Component, OnInit } from '@angular/core';
import { FormGroup,FormControl, Validators } from '@angular/forms';
import { RelationService } from '../relation.service';
// import { Validators } from '@angular/forms/src/validators';  

@Component({
  selector: 'app-leader',
  templateUrl: './leader.component.html',
  styleUrls: ['./leader.component.css']
})
export class LeaderComponent implements OnInit {

  public leader;
  public leaderdata:any= [];
  public updateLeaderId;
  public add:boolean = true;
  public update:boolean = false;
  public validate = true;

  constructor(private relationservice:RelationService) {
  }

  ngOnInit() {
    this.leader = new FormGroup({
      teamleader: new FormControl('', Validators.required)
    });
  }
    
  public addLeader(value){
    if(value.teamleader != '' &&  value.teamleader != null ){
      this.relationservice.saveLeader(value).then((result)=>{
        this.leaderdata = result;
        this.leader.reset();
        })
        this.validate = true;
    }
    else{
      this.validate = false; 
    }
  }

  public checkValidataion(event){
    if(event.target.value)
    {
      this.validate = true;
    }else {
      this.validate = false;
    }
  }
  
  public editLeader(id){
    this.add = false;
    this.update = true;
    const leader = this.relationservice.findLeaderById(id);
    this.leader.setValue({teamleader:leader.lName});
    this.updateLeaderId = leader.l_id;
  }
  public updateLeader(value){
    this.relationservice.updateSaveLeader(value, this.updateLeaderId).then((result)=>{
      this.leaderdata = result;
      this.update = false;
      this.add = true;
    })

    this.leader.reset();
  }
}

